var nodemailer = require('nodemailer');
var fs = require('fs');

var transport = nodemailer.createTransport({
     // in case it is a well known service you can simply name it. No need of host and ports
     // to see the list of well known services you can visit https://github.com/nodemailer/nodemailer-wellknown#supported-services
    service: 'gmail',
    auth: {
        user: 'xxxxxxxxx@gmail.com',
        pass: 'xxxxxxxxxx' // this should be the Application Specific Password else it wont work
    }
});

// obviously you can send normal text emails,
// but html emails are pretty awesome
fs.readFile('test.html', 'utf8', function (err, file) {
    if (err) {
        console.log(err);
    } else {
        transport.sendMail({
            from: 'suryadeep10@gmail.com',
            to: 'suryadeep10@gmail.com',
            subject: 'test',
            html: file
        }, function (err, info) {
            if (err) {
                console.log(err);
            } else {
                console.log(info);
            }
        })

    }
})
